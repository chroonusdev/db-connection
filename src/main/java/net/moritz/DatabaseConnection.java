package net.moritz;

import java.sql.Driver;
import java.sql.SQLException;
import java.util.List;

/**
 * @author moritz
 */
public class DatabaseConnection
{

    public static void main( String[] args ) throws ClassNotFoundException, SQLException, IllegalAccessException, InstantiationException
    {

        MySQLDatabaseConnector connector = new MySQLDatabaseConnector( "jdbc:mysql://localhost:3306/is_uni",
                "root", "",
                (Class<? extends Driver>) Class.forName( "com.mysql.jdbc.Driver" ) );


        DataTransferObjectList<ProfessorDTO> dtoList = connector.find( "professor", new ProfessorTransformer() );

        List<ProfessorDTO> list = dtoList.findAll( professorDTO -> professorDTO != null );

        list.forEach( dto -> System.out.println( dto.getName() ) );

    }
}
