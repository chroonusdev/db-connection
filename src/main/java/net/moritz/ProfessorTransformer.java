package net.moritz;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author moritz
 */
public class ProfessorTransformer extends AbstractTransformer<ProfessorDTO>
{
    @Override
    public DataTransferObjectList<ProfessorDTO> transform( ResultSet resultSet ) throws SQLException
    {
        DataTransferObjectList<ProfessorDTO> list = new DataTransferObjectList<>();

        while ( resultSet.next() )
        {
            ProfessorDTO message = new ProfessorDTO(
                    resultSet.getInt( "PersNr" ),
                    resultSet.getString( "name" ) );

            list.add( message );
        }

        return list;
    }
}
