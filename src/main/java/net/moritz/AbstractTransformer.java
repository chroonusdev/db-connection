package net.moritz;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author moritz
 */
public abstract class AbstractTransformer<T extends DataTransferObject>
{
    public abstract DataTransferObjectList<T> transform( ResultSet resultSet ) throws SQLException;
}

