package net.moritz;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author moritz
 */
public class DataTransferObjectList<T extends DataTransferObject>
{

    private List<T> dtoList;


    public DataTransferObjectList()
    {
        this.dtoList = new ArrayList<>();
    }

    public DataTransferObjectList( List<T> dtoList )
    {
        this.dtoList = dtoList;
    }

    public T findFirst( Predicate<T> predicate )
    {
        return dtoList.stream().filter( predicate ).findFirst().orElse( null );
    }

    public List<T> findAll( Predicate<T> predicate )
    {
        return dtoList.stream().filter( predicate ).collect( Collectors.toList() );
    }

    public List<T> asList()
    {
        return dtoList;
    }

    public boolean isEmpty()
    {
        return dtoList.isEmpty();
    }

    public void add( T dto )
    {
        dtoList.add( dto );
    }

    public void remove( T dto )
    {
        dtoList.remove( dto );
    }
}
