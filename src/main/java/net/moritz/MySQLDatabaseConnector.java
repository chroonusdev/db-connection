package net.moritz;

import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.util.StringUtils;

import javax.sql.DataSource;
import java.sql.*;

/**
 * @author moritz
 */
public class MySQLDatabaseConnector
{

    private final String url;
    private final String username;
    private final String password;

    private DataSource dataSource;

    public MySQLDatabaseConnector( String url, String username, String password, Class<? extends Driver> driver )
    {
        this.url = url;
        this.username = username;
        this.password = password;

        this.dataSource = buildDataSource( url, username, password, driver );
    }

    public SimpleDriverDataSource buildDataSource( String url, String username, String password, Class<? extends Driver> driver )
    {
        SimpleDriverDataSource simpleDriverDataSource = new SimpleDriverDataSource();
        simpleDriverDataSource.setUrl( url );
        simpleDriverDataSource.setUsername( username );
        simpleDriverDataSource.setPassword( password );
        simpleDriverDataSource.setDriverClass( driver );

        return simpleDriverDataSource;
    }

    public <T extends DataTransferObject> DataTransferObjectList<T> executeQuery( String query, AbstractTransformer<T> transformer,
                                                                                  String... arguments ) throws SQLException
    {
        if ( StringUtils.countOccurrencesOf( query, "?" ) != arguments.length )
        {
            throw new SQLException( "Arguments and query do not match" );
        }

        try ( Connection connection = dataSource.getConnection() )
        {
            try ( PreparedStatement preparedStatement = connection.prepareStatement( query ) )
            {
                for ( int i = 0; i < arguments.length; i++ )
                {
                    preparedStatement.setString( i + 1, arguments[i] );
                }

                try ( ResultSet resultSet = preparedStatement.executeQuery() )
                {
                    return transformer.transform( resultSet );
                }
            }
        }
    }

    public <T extends DataTransferObject> DataTransferObjectList<T> executeQuery( String query,
                                                                                  AbstractTransformer<T> transformer ) throws SQLException
    {
        try ( Connection connection = dataSource.getConnection() )
        {
            try ( PreparedStatement preparedStatement = connection.prepareStatement( query ) )
            {
                try ( ResultSet resultSet = preparedStatement.executeQuery() )
                {
                    return transformer.transform( resultSet );
                }
            }
        }
    }

    public <T extends DataTransferObject> DataTransferObjectList<T> find( String tableName,
                                                                          AbstractTransformer<T> transformer ) throws SQLException
    {
        return executeQuery( "SELECT * FROM " + tableName, transformer );
    }

    public <T extends DataTransferObject> DataTransferObjectList<T> findWithCondition( String key, String value,
                                                                                       String tableName,
                                                                                       AbstractTransformer<T> transformer ) throws SQLException
    {
        return executeQuery( "SELECT * FROM " + tableName + " WHERE " + key + "=?", transformer, value );
    }
}
