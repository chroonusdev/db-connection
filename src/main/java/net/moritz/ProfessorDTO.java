package net.moritz;

/**
 * @author moritz
 */
public class ProfessorDTO implements DataTransferObject
{

    private int persNr;
    private String name;

    public ProfessorDTO( int persNr, String name )
    {
        this.persNr = persNr;
        this.name = name;
    }

    public int getPersNr()
    {
        return persNr;
    }

    public String getName()
    {
        return name;
    }
}
